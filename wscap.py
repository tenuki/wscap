#!/usr/bin/env python
# -*- coding: utf-8 -*-
import unittest
import struct
from scapy.all import *

def Xor(a_string, b_string):
    return ''.join( chr(ord(x)^ord(y)) for x,y in zip(a_string, b_string) )


class WS:
    """https://tools.ietf.org/html/rfc6455#section-1.2"""

    @classmethod
    def SuperFilter(cls, pkt, port=None):
        if not hasattr(pkt, "load"):
            return False
        if pkt.load=="":
            return False
        if pkt.load[0]!=chr(0x81):
            return False
        return True

    @classmethod
    def Filter(cls, pkt, port=None):
        if not hasattr(pkt, "proto"):
            return False
        if pkt.proto!=6:
            return False
        if not (port is None):
            if not hasattr(pkt, "sport") or not hasattr(pkt, "dport"):
                return False
            if not port in (pkt.sport, pkt.dport):
                return False
        if not hasattr(pkt, "load"):
            return False
        if pkt.load=="":
            return False
        if pkt.load[0]!=chr(0x81):
            return False
        return True

    @classmethod
    def FromStr(cls, a_string):
        return cls(''.join(chr(int(x,16)) for x in a_string.split(":") ))
    
    @classmethod
    def FromHexStr(cls, a_string):
        s=''
        while a_string:
            x, a_string = a_string[:2], a_string[2:] 
            s+= chr(int(x,16))
        return cls(s)

    def __init__(self, data):
        self.data = data

    @property
    def masked(self):
        return bool(ord(self.data[1]) & 2**7==2**7)

    @property
    def lenlen(self):
        _len = ord(self.data[1]) & ((2**7)-1)
        if _len<=125:
            return 1
        if _len==126:
            return 3
        # this should be 9:
        return 3

    @property
    def length(self):
        _len = ord(self.data[1]) & ((2**7)-1)
        if _len<=125:
            return _len
        if _len==126:
            return struct.unpack("!H", self.data[2:4])[0]
        return struct.unpack("!H", self.data[2:4])[0]

    @property
    def mask_offset(self):
        return 1+self.lenlen

    @property
    def payload_offset(self):
        return self.mask_offset + (4 if self.masked else 0)

    @property
    def mask(self):
        ofs = self.mask_offset
        return self.data[ofs:ofs+4]

    def payload(self):
        payload = self.data[self.payload_offset:]
        if self.masked:
            remaining, payload = payload, ""
            while len(remaining)>=4:
                rem, remaining = remaining[:4], remaining[4:]
                payload += Xor(rem, self.mask)
            payload += Xor(remaining, (self.mask[:len(remaining)]))
        return payload

    def show(self):
        return (">" if self.masked else "<") +" "+ self.payload()


class MyPkt:
    def __init__(self, pkt):
        self.ws = None
        self.pkt = pkt
        try:
            pkt[1]
            self.ws = WS(pkt.load) if WS.Filter(pkt) else None
        except:
            pass

    def show(self):
        return "" if self.ws is None else self.ws.show()

    def sep(self):
        return "A|" if self.ws is None else " |"

    def __str__(self):
        data = self.ws.show() if self.ws else ""
        load = ''.join( '%02x'%ord(x) for x in self.pkt.load) if self.ws else ''
        return "%s:%d -> %s:%d  %s"%(self.pkt[1].src, self.pkt.sport, self.pkt[1].dst, self.pkt.dport, self.sep() + self.show())

    def full(self):
        data = self.ws.show() if self.ws else ""
        load = ''.join( '%02x'%ord(x) for x in self.pkt.load) if self.ws else ''
        print(self)
        print("%s:%d -> %s:%d  %s"%(self.pkt[1].src, self.pkt.sport, self.pkt[1].dst, self.pkt.dport, self.sep() + load ))



class TestBasic(unittest.TestCase):
    def test_reply_137(self):
        s = "81:39:28:31:34:30:36:32:34:33:30:30:32:35:35:31:30:34:29:20:59:6f:75:20:73:61:69:64:3a:20:7b:22:74:79:70:65:22:3a:22:69:6e:66:6f:22:2c:22:63:6f:75:6e:74:65:72:22:3a:31:33:37:7d"
        p = WS.FromStr(s)
        self.assertEqual(p.length, 57)
        self.assertEqual(p.lenlen, 1)
        self.assertEqual(p.masked, False)
        self.assertEqual(p.payload(), '(140624300255104) You said: {"type":"info","counter":137}')

    def test_original_137(self):
        s = "81:9d:c5:3e:a6:c3:be:1c:d2:ba:b5:5b:84:f9:e7:57:c8:a5:aa:1c:8a:e1:a6:51:d3:ad:b1:5b:d4:e1:ff:0f:95:f4:b8"
        p = WS.FromStr(s)
        self.assertEqual(p.length, 29)
        self.assertEqual(p.lenlen, 1)
        self.assertEqual(p.masked, True)
        self.assertEqual(p.payload(), '{"type":"info","counter":137}')

class TestBig(unittest.TestCase):
    def test_one_big(self):
        s = "81fe00970883477073a1330978e6654a2aea291667a16b526bec321e7ce6355232b2765c2ae12e176fe6355232a106234cc506234cc506234cc506234cc506234cc506345bc506234cc5032349c506234cc506234cc506234cc506234cc514314cc506234cc5032349c506234cc506234cc506234cc5032349c506234cc506234cc506234cc506234cc506234cc506234ec706364cd006234cc506234ea13a"
        p = WS.FromHexStr(s)
        self.assertEqual(p.length, 151)
        self.assertEqual(p.lenlen, 3)
        self.assertEqual(p.masked, True)
        self.assertEqual(p.mask, '\x08\x83Gp')
        self.assertEqual(p.payload(), '{"type":"info","counter":11,"bigger":"ASDFASDFASDFASDFASDFADSFASDFDSAFASDFASDFASDFASDFSADFASDFDSAFASDFASDFASDFDSAFASDFASDFASDFASDFASDFASFDAFDSASDFASF"}')

    def test_big_two(self):
        s = "817e00b3283134303238363739303839353438382920596f7520736169643a207b2274797065223a22696e666f222c22636f756e746572223a31312c22626967676572223a22415344464153444641534446415344464153444641445346415344464453414641534446415344464153444641534446534144464153444644534146415344464153444641534446445341464153444641534446415344464153444641534446415346444146445341534446415346227d"
        p = WS.FromHexStr(s)
        self.assertEqual(p.length, 179)
        self.assertEqual(p.lenlen, 3)
        self.assertEqual(p.masked, False)
        self.assertEqual(p.payload(), '(140286790895488) You said: {"type":"info","counter":11,"bigger":"ASDFASDFASDFASDFASDFADSFASDFDSAFASDFASDFASDFASDFSADFASDFDSAFASDFASDFASDFDSAFASDFASDFASDFASDFASDFASFDAFDSASDFASF"}')


def scapy():
    def myprint(pkt):
        if WS.SuperFilter(pkt):
            print(MyPkt(pkt))
            #MyPkt(pkt).full()

    filter = " ".join(sys.argv[1:])
    print("Filter will be: '%s'"%filter)
    sniff(prn=myprint, filter=filter, store=0)


if __name__=="__main__":
    if "test" in sys.argv:
        unittest.main()
    else:
        scapy()