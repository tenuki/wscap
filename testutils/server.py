import tornado.ioloop
import tornado.web
import tornado.websocket

import sys


class QuitHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("bye!")
        sys.exit(-1)

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("""
<html>

<a href="/quit">quit</a>



<script>

let socket = new WebSocket("ws://"+window.location.hostname+":8888/echo");
let log = console.log;

socket.onopen = function(e) {
  log("[open] Connection established");
  socket.send("My name is John");
  setTimeout(run, 5000);
};

let counter = 0;

function run() {
    socket.send( JSON.stringify({type: "info", counter: counter, bigger:"ASDFASDFASDFASDFASDFADSFASDFDSAFASDFASDFASDFASDFSADFASDFDSAFASDFASDFASDFDSAFASDFASDFASDFASDFASDFASFDAFDSASDFASF"}) );
    counter += 1;
    setTimeout(run, 5000);
}

socket.onmessage = function(event) {
  log(`[message] Data received from server: ${event.data}`);
};

socket.onclose = function(event) {
  if (event.wasClean) {
    log(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
  } else {
    // e.g. server process killed or network down
    // event.code is usually 1006 in this case
    log('[close] Connection died');
  }
};


</script>
</html>        
""")


class EchoWebSocket(tornado.websocket.WebSocketHandler):
    def open(self):
        print("WebSocket opened")
        self.write_message(u"(%d) Hello!"%(id(self)))

    def on_message(self, message):
        self.write_message(u"(%d) You said: %s" % (id(self), message))

    def on_close(self):
        print("WebSocket closed")


def make_app():
    return tornado.web.Application([
        (r"/quit", QuitHandler),
        (r"/echo", EchoWebSocket),
        (r"/", MainHandler),
    ])

if __name__ == "__main__":
    app = make_app()
    app.listen(8888, '0.0.0.0')
    tornado.ioloop.IOLoop.current().start()

