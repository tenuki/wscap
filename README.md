# wscap: online display websockets data

```bash
### setup
$ git clone https://github.com/secdev/scapy.git 
$ sudo python wscap.py [port 80]
```

Depends on: [https://github.com/secdev/scapy](scapy) installed and requires higher priviledges to run.

- Also, on windows depends as scapy on win-pcap or libpcap. 
- On linux depends on tcpdump. 
- On OSX on libpcap.

For more precise information check: https://scapy.readthedocs.io/en/latest/installation.html


### Some tests included:

```bash
$ python -m unittest wscap
```

### Produce WS traffic

To produce some ws data run this server on other host:
```bash
$ cd testutils
$ pip install -r requirements
$ python server.py
```

From your workstation connect to:  http://server:8888/

This will produce a constant flow of WS data.

